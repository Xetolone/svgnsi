# svgnsi

Bibliothèque très simple pour dessiner en SVG à partir de Python avec des élèves de lycée.

## fonctionnalités

- ligne
- rectangle
- cercle
- texte
- chemin
