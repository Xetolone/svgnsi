class Draw:

	def __init__(self, width, height):
		self.width = width
		self.height = height
		self.body = ""
		self.police = ""
		self.template = """<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 {} {}">
\t{}
</svg>"""
		self.template_police = """<defs>
\t\t<style>
\t\t\t{}
\t\t</style>
\t</defs>
\t"""


	def generate(self):
		"""
			Crée le svg.
		"""
		if self.police != "":
			self.body = self.template_police.format(self.police.strip()) + self.body
		return self.template.format(self.width, self.height, self.body.strip())


	def output(self):
		"""
			Affiche le code de l'image dans le shell
		"""
		print(self.generate())


	def save(self, filename):
		"""
			Enregistre l'image dans le fichier filename
		"""
		f = open(filename, 'w')
		f.write(self.generate())
		f.close()


	def addPolice(self, url):
		"""
			Ajoute une police grâce à son url dans Google font.
			Il faut aller sur cette url https://fonts.google.com sélectionner
			une police et récupérer l'url dans le @import.
		"""
		t = '@import url("{}");'
		self.police += t.format(url.replace("&","&amp;")) + "\n\t\t\t"


	def rectangle(self, x, y, width, height, stroke = "", strokewidth = "", fill = ""):
		"""
			Trace un rectangle.

			x : abscisse du point haut gauche
			y : ordonnée du point haut gauche
			width : largeur du rectangle
			height : hauteur de rectangle
			stroke : couleur du trait (ex : "#004488" ; défaut : "none")
			strokewidth : épaisseur de trait avec l'unité (ex : "2px" ; défaut : "1px")
			fill : couleur de remplissage (ex : "#004488" ; défaut : "black")

			Note : par défaut un rectangle sans contour et rempli de noir est dessiné.
		"""

		t = '<rect x="{}" y="{}" width="{}" height="{}"{}/>'

		style_content = ""
		if stroke != "":
			style_content += ' stroke: ' + stroke + ';'
		if strokewidth != "":
			style_content += ' stroke-width: ' + strokewidth + ';'
		if fill != "":
			style_content += ' fill: ' + fill + ';'

		if style_content != "":
			style= ' style="' + style_content[1:] + '"'
		else:
			style = ""

		self.body += t.format(x, y, width, height, style) + "\n\t"


	def line(self, x1, y1, x2, y2, stroke = "", strokewidth = ""):
		"""
			Trace une ligne.

			x1 : abscisse du premier point
			y1 : ordonnée du premier point
			x2 : abscisse du deuxième point
			y2 : ordonnée du deuxième point
			stroke : couleur du trait (défaut : "none")
			strokewidth : épaisseur de trait (défaut : "1px")

			Note : par défaut, la ligne n'est pas visible.
		"""

		t = '<line x1="{}" y1="{}" x2="{}" y2="{}"{}/>'

		style_content = ""
		if stroke != "":
			style_content += ' stroke: ' + stroke + ';'
		if strokewidth != "":
			style_content += ' stroke-width: ' + strokewidth + ';'

		if style_content != "":
			style= ' style="' + style_content[1:] + '"'
		else:
			style = ""

		self.body += t.format(x1, y1, x2, y2, style) + "\n\t"


	def polyline(self, points, stroke = "", strokewidth = "", fill = ""):
		"""
			Relie une liste de points par des lignes.

			points : liste de points séparés par des espaces
			stroke : couleur du trait (défaut : "none")
			strokewidth : épaisseur de trait (défaut : "1px")
			fill : couleur de remplissage (ex : "#004488" ; défaut : "black")

			Note : par défaut, la ligne n'est pas visible.
		"""

		t = '<polyline points="{}"{}/>'

		style_content = ""
		if stroke != "":
			style_content += ' stroke: ' + stroke + ';'
		if strokewidth != "":
			style_content += ' stroke-width: ' + strokewidth + ';'
		if fill != "":
			style_content += ' fill: ' + fill + ';'

		if style_content != "":
			style= ' style="' + style_content[1:] + '"'
		else:
			style = ""

		self.body += t.format(points, style) + "\n\t"
	


	def circle(self, cx, cy, r, stroke = "", strokewidth = "", fill = ""):
		"""
			Trace un cercle.

			cx : abscisse du centre
			cy : ordonnée du centre
			r : rayon du cercle
			stroke : couleur du trait (ex : "#004488" ; défaut : "none")
			strokewidth : épaisseur de trait avec l'unité (ex : "2px" ; défaut : "1px")
			fill : couleur de remplissage (ex : "#004488" ; défaut : "black")

			Note : par défaut un cercle sans contour et rempli de noir est dessiné.
		"""

		t = '<circle cx="{}" cy="{}" r="{}"{}/>'

		style_content = ""
		if stroke != "":
			style_content += ' stroke: ' + stroke + ';'
		if strokewidth != "":
			style_content += ' stroke-width: ' + strokewidth + ';'
		if fill != "":
			style_content += ' fill: ' + fill + ';'

		if style_content != "":
			style= ' style="' + style_content[1:] + '"'
		else:
			style = ""

		self.body += t.format(cx, cy, r, style) + "\n\t"


	def text(self, x, y, s, stroke = "", strokewidth = "", fill = "", fontsize = "", fontfamily = "", textanchor = "", rotate = ""):
		"""
			Écrit un texte.

			x : abscisse du point en bas à gauche du texte
			y : ordonnée du point en bas à gauche du texte
			s : le texte à afficher
			stroke : couleur du trait (défaut : "none")
			strokewidth : épaisseur de trait (défaut : "1px")
			fill : couleur de remplissage (défaut : "black")
			fontsize : taille de police (défaut : "16px")
			fontfamily : police
			textanchor : position d'attache du texte en abscisse (start | middle | end; défaut : start)
			rotate : angle de rotation du texte par rapport au point d'attache. Sens trigonométrique en degré, sans unité.
		"""

		t='<text x="{}" y="{}"{}{}>{}</text>'

		style_content = ""
		if stroke != "":
			style_content += ' stroke: ' + stroke + ';'
		if strokewidth != "":
			style_content += ' stroke-width: ' + strokewidth + ';'
		if fill != "":
			style_content += ' fill: ' + fill + ';'
		if fontsize != "":
			style_content += ' font-size: ' + fontsize + ';'
		if fontfamily != "":
			style_content += ' font-family: ' + fontfamily + ';'
		if textanchor != "":
			style_content += ' text-anchor: ' + textanchor + ';'
		if rotate != "":
			rotate = - int(rotate)
			transform = ' transform="rotate(' + str(rotate) + ',' + str(x) + ',' + str(y) + ')"'
		else:
			transform = ""

		if style_content != "":
			style= ' style="' + style_content[1:] + '"'
		else:
			style = ""

		self.body += t.format(x, y, transform, style, s) + "\n\t"


	def path(self, d, stroke = "", strokewidth = "", fill = ""):
		"""
			Trace un chemin.

			d : chemin
			stroke : couleur du trait (ex : "#004488" ; défaut : "none")
			strokewidth : épaisseur de trait avec l'unité (ex : "2px" ; défaut : "1px")
			fill : couleur de remplissage (ex : "#004488" ; défaut : "black")

			Note : il faut connaitre les commandes qui permettent de tracer un chemin en SVG (https://developer.mozilla.org/fr/docs/Web/SVG/Tutorial/Paths).
		"""

		t = '<path d="{}"{}/>'

		style_content = ""
		if stroke != "":
			style_content += ' stroke: ' + stroke + ';'
		if strokewidth != "":
			style_content += ' stroke-width: ' + strokewidth + ';'
		if fill != "":
			style_content += ' fill: ' + fill + ';'

		if style_content != "":
			style= ' style="' + style_content[1:] + '"'
		else:
			style = ""

		self.body += t.format(d, style) + "\n\t"



if __name__ == "__main__":
	# Exemple de creation d'image
	# Image de 200 pixels de large et 150 pixeles de hauteur
	test = Draw(200,150)
	# Rectangle
	test.rectangle(0,0,20,30, "#222222", "2px", "#00EE00")
	# Ligne
	test.line(100,50,150,50,"#000000")
	# Cercle
	test.circle(50,50,30, "#111111", "1px", "red")
	# Texte avec la police par defaut
	test.text(10,20,"Police par défaut", "", "", "#222222", "20px")
	# Ajout d'une nouvelle police
	test.addPolice("https://fonts.googleapis.com/css2?family=RocknRoll+One&display=swap")
	# Texte avec la nouvelle police
	test.text(10,80,"Police externe", "", "", "#222222", "20px", "RocknRoll One")
	# Texte incliné
	test.text(10,50,"texte penché", "", "", "#222222", "10px", "", "", 20)
	# Exemple de chemin
	test.path("M10 80 Q 52.5 10, 95 80 T 180 80", "black", "", "none")
	# Exemple de polyline
	test.polyline("10,110 50,90 70,120", "#FF0000", "", "none")
	# Affichage dans la console (facultatif)
	test.output()
	# Sauvegarde du fichier
	test.save("exemple.svg")
